package id.ac.ui.cs.advprog.tutorial3.composite.controller;

import id.ac.ui.cs.advprog.tutorial3.composite.service.GuildServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = GuildController.class)
public class GuildControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GuildServiceImpl guildService;

    @Test
    public void whenGuildMemberUrlIsAccessedItShouldContainCorrectMemberListAndMemberTree() throws Exception {
        mockMvc.perform(get("/guild/member"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("memberlist"))
                .andExpect(model().attributeExists("membertree"))
                .andExpect(view().name("composite/member"));
    }

    @Test
    public void whenMemberAddURLIsAccessedItShouldAddNewMember() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("childname", "Budi");
        params.add("childrole", "Swordsman");
        params.add("childtype", "primary");
        params.add("parent", "Heathcliff Master");

        mockMvc.perform(post("/guild/add").params(params))
                .andExpect(handler().methodName("add"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("member"));
    }

    @Test
    public void whenMemberRemoveURLIsAccessedItShouldRemoveMember() throws Exception {
        MultiValueMap<String, String> postParams = new LinkedMultiValueMap<>();
        postParams.add("childname", "Budi");
        postParams.add("childrole", "Swordsman");
        postParams.add("childtype", "primary");
        postParams.add("parent", "Heathcliff Master");

        mockMvc.perform(post("/guild/add").params(postParams));

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("child", "Budi Swordsman");
        params.add("parent", "Heathcliff Master");

        mockMvc.perform(post("/guild/remove").params(params))
                .andExpect(handler().methodName("remove"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("member"));
    }
}
