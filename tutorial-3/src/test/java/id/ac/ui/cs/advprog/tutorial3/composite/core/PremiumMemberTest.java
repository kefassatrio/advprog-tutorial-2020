package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals(member.getName(), "Wati");
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals(member.getRole(), "Gold Merchant");
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member newMember = new OrdinaryMember("NewMember", "Ordinary");
        member.addChildMember(newMember);
        assertEquals(1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member premiumMember = new PremiumMember("Koe", "Premium");
        member.addChildMember(premiumMember);
        member.removeChildMember(premiumMember);
        assertEquals(member.getChildMembers().contains(premiumMember), false);
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member newMember1 = new OrdinaryMember("NewMember1", "Ordinary");
        Member newMember2 = new OrdinaryMember("NewMember2", "Ordinary");
        Member newMember3 = new OrdinaryMember("NewMember3", "Ordinary");
        Member newMember4 = new OrdinaryMember("NewMember4", "Ordinary");
        member.addChildMember(newMember1);
        member.addChildMember(newMember2);
        member.addChildMember(newMember3);
        member.addChildMember(newMember4);
        assertEquals(3, member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member guildMaster = new PremiumMember("Eko", "Master");
        Member newMember1 = new OrdinaryMember("NewMember1", "Ordinary");
        Member newMember2 = new OrdinaryMember("NewMember2", "Ordinary");
        Member newMember3 = new OrdinaryMember("NewMember3", "Ordinary");
        Member newMember4 = new OrdinaryMember("NewMember4", "Ordinary");
        guildMaster.addChildMember(newMember1);
        guildMaster.addChildMember(newMember2);
        guildMaster.addChildMember(newMember3);
        guildMaster.addChildMember(newMember4);
        assertEquals(4, guildMaster.getChildMembers().size());
    }
}
