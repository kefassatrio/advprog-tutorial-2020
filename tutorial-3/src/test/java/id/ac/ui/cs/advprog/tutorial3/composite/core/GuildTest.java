package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        //TODO: Complete me
        Member premiumMember = new PremiumMember("Keo", "Premium");
        guild.addMember(guildMaster, premiumMember);
        assertEquals(guild.getMemberList().size(), 2);
    }

    @Test
    public void testMethodRemoveMember() {
        //TODO: Complete me
        Member premiumMember = new PremiumMember("Koe", "Premium");
        guild.addMember(guildMaster, premiumMember);
        guild.removeMember(guildMaster, premiumMember);
        assertEquals(guild.getMemberList().contains(premiumMember), false);
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        //TODO: Complete me
        Member premiumMember = new PremiumMember("Oke", "Premium");
        guild.addMember(guildMaster, premiumMember);
        Member foundMember = guild.getMember("Oke", "Premium");
        assertEquals(foundMember, premiumMember);
    }
}
