package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
        //TODO: Complete me
        String name;
        String role;
        List<Member> childMemberList;

        public PremiumMember(String name, String role){
                this.name = name;
                this.role = role;
                childMemberList = new ArrayList<Member>();
        }
        public String getName(){
                return name;
        }
        public String getRole(){
                return role;
        }
        public void addChildMember(Member member){
                if(role.equals("Master") || childMemberList.size() <3){
                        childMemberList.add(member);
                }
        }

        public void removeChildMember(Member member){
                if(childMemberList.contains(member)){
                        childMemberList.remove(member);
                }
        }

        public List<Member> getChildMembers(){
                return childMemberList;
        }
}
