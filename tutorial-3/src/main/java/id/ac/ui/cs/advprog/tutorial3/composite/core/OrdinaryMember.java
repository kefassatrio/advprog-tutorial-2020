package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
        //TODO: Complete me
        String name;
        String role;
        List<Member> childMemberList;

        public OrdinaryMember(String name, String role){
                this.name = name;
                this.role = role;
                childMemberList = new ArrayList<Member>();
        }
        public String getName(){
                return name;
        }
        public String getRole(){
                return role;
        }
        public void addChildMember(Member member){
                
        }

        public void removeChildMember(Member member){

        }

        public List<Member> getChildMembers(){
                return childMemberList;
        }
}
