package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class RegularUpgrade extends Weapon {

    Weapon weapon;
    int valueUpgrade;

    public RegularUpgrade(Weapon weapon) {

        this.weapon= weapon;
        valueUpgrade = ThreadLocalRandom.current().nextInt(1, 5 + 1);

    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 1-5 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        if(weapon == null){
            return 0 + valueUpgrade;
        }
        return weapon.getWeaponValue() + valueUpgrade;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Regular upgraded " + weapon.getDescription();
    }
}
