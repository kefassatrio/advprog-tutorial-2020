package id.ac.ui.cs.advprog.tutorial3.composite.controller;

import id.ac.ui.cs.advprog.tutorial3.composite.core.Member;
import id.ac.ui.cs.advprog.tutorial3.composite.service.GuildService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(path = "/guild")
public class GuildController {

    @Autowired
    private GuildService guildService;

    @RequestMapping(path = "/member", method = RequestMethod.GET)
    public String member(Model model) {
        model.addAttribute("membertree", guildService.getMemberHierarchy());
        model.addAttribute("memberlist", guildService.getMemberList());
        return "composite/member";
    }

    @RequestMapping(path = "/add", method = RequestMethod.POST)
    public String add(@RequestParam("parent") String parent, @RequestParam("childname") String childName,
                      @RequestParam("childrole") String childRole, @RequestParam("childtype") String childType) {
        guildService.addMember(parent, childName, childRole, childType);
        return "redirect:member";
    }

    @RequestMapping(path = "/remove", method = RequestMethod.POST)
    public String remove(@RequestParam("parent") String parent, @RequestParam("child") String child) {
        guildService.removeMember(parent, child);
        return "redirect:member";
    }
}
