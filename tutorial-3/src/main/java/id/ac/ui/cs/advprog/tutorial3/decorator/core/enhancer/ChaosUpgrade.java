package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;
    int valueUpgrade;

    public ChaosUpgrade(Weapon weapon) {
        this.weapon = weapon;
        valueUpgrade = ThreadLocalRandom.current().nextInt(50, 55 + 1);
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        if(weapon == null){
            return 0 + valueUpgrade;
        }
        return weapon.getWeaponValue() + valueUpgrade;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Chaos upgraded " + weapon.getDescription();
    }
}
