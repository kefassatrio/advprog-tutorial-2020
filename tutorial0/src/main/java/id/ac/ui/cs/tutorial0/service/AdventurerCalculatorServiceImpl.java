package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class AdventurerCalculatorServiceImpl implements AdventurerCalculatorService {

    @Override
    public String countPowerPotensialFromBirthYear(int birthYear) {
        int rawAge = getRawAge(birthYear);
        int power = 0;
        if (rawAge<30) {
            power = rawAge*2000;
        } else if (rawAge <50) {
            power = rawAge*2250;
        } else {
            power = rawAge*5000;
        }
        if(power <= 20000) return "C class";
        if(power <= 100000) return "B class";
        return "A class";


    }

    private int getRawAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear-birthYear;
    }
}
