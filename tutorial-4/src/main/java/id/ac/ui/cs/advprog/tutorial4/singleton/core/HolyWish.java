package id.ac.ui.cs.advprog.tutorial4.singleton.core;

public class HolyWish {

    private String wish;

    // TODO complete me with any Singleton approach
    private static HolyWish holyWish;

    public String getWish() {
        return wish;
    }

    private HolyWish(){
        wish = "";
    }

    public void setWish(String wish) {
        this.wish = wish;
    }

    @Override
    public String toString() {
        return wish;
    }
    
    public static HolyWish getInstance(){
        if(holyWish == null) holyWish = new HolyWish();
        return holyWish;
    }
}
