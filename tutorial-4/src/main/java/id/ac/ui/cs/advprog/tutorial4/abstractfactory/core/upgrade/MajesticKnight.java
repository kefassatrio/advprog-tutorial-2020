package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;

public class MajesticKnight extends Knight {

    public MajesticKnight(Armory armory) {
        this.armory = armory;
        this.setName("Majestic Knight");
    }

    @Override
    public void prepare() {
        // TODO complete me
        armor = armory.craftArmor();
        weapon = armory.craftWeapon();
        skill = armory.learnSkill();
    }
}
