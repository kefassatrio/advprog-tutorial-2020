package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;

public class MetalClusterKnight extends Knight {

    public MetalClusterKnight(Armory armory) {
        this.armory = armory;
        this.setName("Metal Cluster Knight");
    }

    @Override
    public void prepare() {
        // TODO complete me
        armor = armory.craftArmor();
        weapon = armory.craftWeapon();
        skill = armory.learnSkill();
    }
}
