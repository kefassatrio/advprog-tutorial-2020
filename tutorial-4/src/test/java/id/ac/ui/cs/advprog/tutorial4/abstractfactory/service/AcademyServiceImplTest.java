package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

public class AcademyServiceImplTest {

    private AcademyRepository academyRepository;

    private AcademyServiceImpl academyService;

    // TODO create tests
    @BeforeEach
    public void setUp() {
      academyRepository = new AcademyRepository();
      academyService = new AcademyServiceImpl(academyRepository);
    }

    @Test
    public void testBanyakAcademy(){
        assertEquals(academyService.getKnightAcademies().size(), 2);
    }

    @Test
    public void testProducedKnightNotNull(){
        academyService.produceKnight("Drangleic", "synthetic");
        Knight knight = academyService.getKnight();
        assertNotNull(knight);
    }
}
