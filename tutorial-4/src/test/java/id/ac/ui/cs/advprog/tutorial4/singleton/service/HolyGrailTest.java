package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class HolyGrailTest {
    HolyGrail holyGrail;
    // TODO create tests
    @BeforeEach
    public void setUp() {
        holyGrail = new HolyGrail();
    }

    @Test
    public void testMakeAWish(){
        holyGrail.makeAWish("wish");
        assertEquals(holyGrail.getHolyWish().getWish(), "wish");
    }
    @Test
    public void testGetHolyWish(){
        assertNotNull(holyGrail.getHolyWish());
    }
}
