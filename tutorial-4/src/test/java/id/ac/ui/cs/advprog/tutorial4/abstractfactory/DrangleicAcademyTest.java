package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class DrangleicAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        drangleicAcademy = new DrangleicAcademy();
        majesticKnight = drangleicAcademy.getKnight("majestic");
        metalClusterKnight = drangleicAcademy.getKnight("metal cluster");
        syntheticKnight = drangleicAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertNotNull(majesticKnight);
        assertNotNull(metalClusterKnight);
        assertNotNull(syntheticKnight);

    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        majesticKnight.setName("majestic");
        metalClusterKnight.setName("metalCluster");
        syntheticKnight.setName("synthetic");

        assertEquals(majesticKnight.getName(), "majestic");
        assertEquals(metalClusterKnight.getName(), "metalCluster");
        assertEquals(syntheticKnight.getName(), "synthetic");
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        assertEquals(majesticKnight.getArmor().getDescription(), "veri metallic armor");
        assertEquals(metalClusterKnight.getArmor().getDescription(), "veri metallic armor");
        assertEquals(syntheticKnight.getArmor().getDescription(), "veri metallic armor");
        
        assertEquals(majesticKnight.getWeapon().getDescription(), "weapon of a legendary left handed jacker");
        assertEquals(metalClusterKnight.getWeapon().getDescription(), "weapon of a legendary left handed jacker");
        assertEquals(syntheticKnight.getWeapon().getDescription(), "weapon of a legendary left handed jacker");

        assertEquals(majesticKnight.getSkill().getDescription(), "skill from a thousand painful years");
        assertEquals(metalClusterKnight.getSkill().getDescription(), "skill from a thousand painful years");
        assertEquals(syntheticKnight.getSkill().getDescription(), "skill from a thousand painful years");
    }
}
