package id.ac.ui.cs.advprog.tutorial2.command.core.spell;
import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> spells;

    public ChainSpell(ArrayList<Spell> spells){
        this.spells = spells;
    }
    public void cast(){
        for(int i = 0; i<this.spells.size(); i++){
            spells.get(i).cast();
        }
    }

    public void undo(){
        if(spells.size()>0){
            for(int i = this.spells.size()-1; i>=0; i--){
                spells.get(i).cast();
            }
        }
    }
    
    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
