package id.ac.ui.cs.advprog.tutorial5.core;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class SoulTest {

    private Soul soul;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        soul = new Soul("gitopotig", 69, "M", "Villager");
    }

    @Test
    void getName() {
        assertEquals("gitopotig",soul.getName());
    }

    @Test
    void setName() {
        soul.setName("notgitopotig");
        assertEquals("notgitopotig",soul.getName());
    }

    @Test
    void getAge() {
        assertEquals(69,soul.getAge());
    }

    @Test
    void setAge() {
        soul.setAge(1);
        assertEquals(1,soul.getAge());
    }

    @Test
    void getId() {
        assertEquals(0,soul.getId());
    }

    @Test
    void setId() {
        soul.setId(5);
        assertEquals(5,soul.getId());
    }

    @Test
    void getGender() {
        assertEquals("M",soul.getGender());
    }

    @Test
    void setGender() {
        soul.setGender("F");
        assertEquals("F",soul.getGender());
    }

    @Test
    void getOccupation() {
        assertEquals("Villager",soul.getOccupation());
    }

    @Test
    void setOccupation() {
        soul.setOccupation("wolf");
        assertEquals("wolf",soul.getOccupation());
    }




}
